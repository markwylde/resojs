package main

import (
	"github.com/klauspost/reedsolomon"
	"github.com/gopherjs/gopherjs/js"
	"github.com/augustoroman/promise"
	"fmt"
	"os"
	"io"
	"bufio"
	"bytes"
)

var dataShards = 4
var parShards = 2

func main() {
	js.Global.Set("gop", map[string]interface{}{
		"encodeString": encodeString,
		"decodeString": decodeString,
		"encode": encode,
		"decode": decode,
	})
}

func encode(byteArray *js.Object) *js.Object {
	var p promise.Promise

	b := js.Global.Get("Uint8Array").New(byteArray).Interface().([]byte)

	enc, err := reedsolomon.New(dataShards, parShards)

	shards, err := enc.Split(b)
	checkErr(err)

	enc.Encode(shards)

	checkErr(err)

	p.Resolve(shards)

  return p.Js()
}

func decode(shards [][]uint8) *js.Object {
	var p promise.Promise

	go func() {
		enc, err := reedsolomon.New(dataShards, parShards)

		// Verify the shards
		ok, err := enc.Verify(shards)
		if ok {
			fmt.Println("No reconstruction needed")
		} else {
			fmt.Println("Verification failed. Reconstructing data")
			err = enc.Reconstruct(shards)
			if err != nil {
				fmt.Println("Reconstruct failed -", err)
			}
			ok, err = enc.Verify(shards)
			if !ok {
				fmt.Println("Verification failed after reconstruction, data likely corrupted.")
			}
			checkErr(err)
		}

		var bfr bytes.Buffer
    wtr := bufio.NewWriter(&bfr)

		err = enc.Join(wtr, shards, len(shards[0]) * dataShards)
		checkErr(err)

		wtr.Flush()

    rdr := bufio.NewReader(&bfr)

		//js.Global.Set("gos", StreamToString(rdr))
		p.Resolve(StreamToByte(rdr))

	}()

  return p.Js()
}

func encodeString(text string) *js.Object {
	var p promise.Promise

	enc, err := reedsolomon.New(dataShards, parShards)

	byteArray := []byte(text)

	//js.Global.Set("borg", string(byteArray[:]))

	shards, err := enc.Split(byteArray)
	checkErr(err)

	enc.Encode(shards)

	checkErr(err)

	p.Resolve(shards)

  return p.Js()
}

func decodeString(shards [][]uint8) *js.Object {
	var p promise.Promise

	go func() {
		enc, err := reedsolomon.New(dataShards, parShards)

		// Verify the shards
		ok, err := enc.Verify(shards)
		if ok {
			fmt.Println("No reconstruction needed")
		} else {
			fmt.Println("Verification failed. Reconstructing data")
			err = enc.Reconstruct(shards)
			if err != nil {
				fmt.Println("Reconstruct failed -", err)
			}
			ok, err = enc.Verify(shards)
			if !ok {
				fmt.Println("Verification failed after reconstruction, data likely corrupted.")
			}
			checkErr(err)
		}

		var bfr bytes.Buffer
    wtr := bufio.NewWriter(&bfr)

		err = enc.Join(wtr, shards, len(shards[0]) * dataShards)
		checkErr(err)

		wtr.Flush()

    rdr := bufio.NewReader(&bfr)

		//js.Global.Set("gos", StreamToString(rdr))
		p.Resolve(StreamToString(rdr))

	}()

  return p.Js()
}

func checkErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s", err.Error())
	}
}





func StreamToByte(stream io.Reader) []byte {
  buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.Bytes()
}

func StreamToString(stream io.Reader) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.String()
}
