export GOPATH=$(pwd)

echo "Building reedsolomon.go"
time ./bin/gopherjs build reedsolomon.go -m

echo "Minifing reedsolomon.js"
time uglifyjs --screw-ie8 --compress --stats --verbose -m -o reedsolomon.min.js reedsolomon.js

echo "Finished"
